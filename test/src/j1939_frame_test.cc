#include <gtest/gtest.h>

#include <iostream>

#include "linsock/can/j1939/frame.h"

TEST(FRAME_TEST, FRAME_CONVERT)
{
  auto frame = linsock::can::j1939::Frame::AddressRequestFrame();
  ASSERT_EQ(frame.GetPDUFormat(), linsock::can::j1939::PF_REQUEST);
  ASSERT_EQ(frame.GetPDUSpecifier(), linsock::can::j1939::ADDRESS_GLOBAL);
  ASSERT_EQ(frame.GetSourceAdderess(), linsock::can::j1939::ADDRESS_NULL);
  ASSERT_EQ(frame.GetData().size(), 3);

  auto can_frame = frame.ToCanFrame();
  ASSERT_EQ(can_frame.can_id, frame.GetHeader());
  ASSERT_EQ(can_frame.can_dlc, 3);

  ASSERT_EQ(can_frame.data[0], 0x00);
  ASSERT_EQ(can_frame.data[1], 0xEE);
  ASSERT_EQ(can_frame.data[2], 0x00);
}

TEST(FRAME_TEST, DATA_CONVERT)
{
  auto data = linsock::can::j1939::Frame::Uint64ToJ1939Data(0xA00C81045A20021BLLU);
  ASSERT_EQ(data.size(), 8);

  ///NOTE: Use opposite order when placed into vector, last byte should be first in data
  linsock::can::j1939::Frame frame{0,0,0,{0x1B,0x02,0x20,0x5A,0x04,0x81,0x0C,0xA0}};
  for(size_t i = 0; i < data.size(); i++)
  {
    ASSERT_EQ(data[i], frame.GetData()[i]);
  }
  ASSERT_EQ(linsock::can::j1939::Frame::J1939DataToUint64(frame.GetData()),linsock::can::j1939::Frame::J1939DataToUint64(data));
}