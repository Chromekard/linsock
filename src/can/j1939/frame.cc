#include "linsock/can/j1939/frame.h"

#include <algorithm>

  //Remember order is [31] ... [0] this is not an array where 0 is the first index
  //First three bits are EFF/RTR/ERR flags then followed by 29-bit message
  //EFF = Frame format flag 1-bit, 0 = stardard 11-bit, 1 = extended 29-bit
  //RTR = Remote transmition request 1-bit, 1 indicates rtr frame with no data
  //ERR = Error frame 1-bit, indicates if the frame is an error frame or not
  //Message data
  //Priority 3-bit
  //Reserved 1bit, Extended data page (EDP) not in use, must be 0
  //Data page 1-bit, expands PGN number if needed
  //PDU format (PF) 8-bit, 0-239 address = PS is destination adress , 240-255 = broadcast and PS is group extensio 
  //PDU specific (PS) 8,bit
  //Source Address 8,bit
  //8AFFBB01
  //100 011 0 0 00000000 10111011 000000000

namespace
{
}

std::vector<uint8_t> linsock::can::j1939::Frame::Uint64ToJ1939Data(const uint64_t data)
{
  std::vector<uint8_t> vector{};
  for(uint8_t i = 0; i < 8; i++)
  {
    vector.emplace_back(static_cast<uint8_t>(data >> i * 8));
  }
  return vector;
}

uint64_t linsock::can::j1939::Frame::J1939DataToUint64(const std::vector<uint8_t> data)
{
  ///TODO: Look into the <endian.h> functions htole64 which convert between endians?
  ///CAN data is little endian
  uint64_t out_data{};
  auto limit = std::min(data.size(), static_cast<size_t>(8));
  for(size_t i = 0; i < limit; i++)
  {
    out_data |= static_cast<uint64_t>(data[i]) << i * 8;
  }
  return out_data;
}

std::vector<uint8_t> linsock::can::j1939::Frame::CanDataToByteVector(const unsigned char array[8], const uint8_t length)
{
  std::vector<uint8_t> vector{std::min(length, static_cast<uint8_t>(8)), 0};
  for (size_t i = 0; i < vector.size(); i++)
  {
    vector[i] = array[i];
  }
  return vector;
}


linsock::can::j1939::Frame::Frame(const uint8_t priority, const bool data_page, 
  const uint8_t pdu_format, const uint8_t pdu_specifier, 
  const uint8_t source_address, const std::vector<uint8_t> data) : 
    priority_(priority), data_page_bit_(data_page),  
    pdu_format_(pdu_format),  pdu_specifier_(pdu_specifier),
    source_address_(source_address), data_(data)
{}

linsock::can::j1939::Frame::Frame(const uint8_t priority, const uint32_t pgn, 
  const uint8_t source_address, const std::vector<uint8_t> data): 
    priority_(priority), data_page_bit_(pgn >> 16),  
    pdu_format_(pgn >> 8),  pdu_specifier_(pgn),
    source_address_(source_address), data_(data)
{

}

linsock::can::j1939::Frame::Frame(const uint32_t header_data, 
  const std::vector<uint8_t> data) : 
    priority_((header_data >> 26) & 0b111), data_page_bit_((header_data >> 24) & 0b1),  
    pdu_format_(header_data >> 16),  pdu_specifier_(header_data >> 8),
    source_address_(header_data), data_(data)
{}


linsock::can::j1939::Frame::Frame(const can_frame& can_frame) : 
  Frame(can_frame.can_id, CanDataToByteVector(can_frame.data, can_frame.can_dlc))
{
  ///TODO: using get header from 
}


bool linsock::can::j1939::Frame::IsBroadCast() const
{
  return pdu_format_ > 0xEF;
}

uint32_t linsock::can::j1939::Frame::GetHeader() const
{
  uint32_t can_id{};
  //General CAN bits =
  can_id |= 1; //Set extended frame bit to true, as J1939 allways uses 29-bit extended frames
  can_id <<= 1;
  can_id |= 0; //Set RTR bit to false, as RTR is always set to zero in J1939.
  can_id <<= 1;
  can_id |= 0; //Set error bit, add to class?

  ///TODO: Add a sepeate function without the 3 header bits above for getting full
  ///29-bit J1939 only header

  //J1939 Frame 29-bit header data
  can_id <<= 3;
  can_id |= priority_; //Set the 3 priority bits
  can_id <<= 1;
  can_id |= 0; //Set Reserved Bit to 0, as Reserved Bit is always set to zero in J1939.
  can_id <<= 1;
  can_id |= data_page_bit_; //Set the data page bit
  can_id <<= 8;
  can_id |= pdu_format_; //set pdu format byte
  can_id <<= 8;
  can_id |= pdu_specifier_; //set pdu specifer byte
  can_id <<= 8;
  can_id |= source_address_; //set source address byte

  //Could possibly reduce to a single line of | (or) statements but then the uint8 would need to be
  //bitshifted into the correct order. In that case it would be best if the type was uint32 instead
  //as it would avoid any casting. But then the problem is should the user specify the information
  //allready bitshifter as the interface might suggest

  return can_id;
}

uint32_t linsock::can::j1939::Frame::GetJ1939Header() const
{
  uint32_t j1939_id{};
  j1939_id |= priority_; //Set the 3 priority bits
  j1939_id <<= 1;
  j1939_id |= 0; //Set Reserved Bit to 0, as Reserved Bit is always set to zero in J1939.
  j1939_id <<= 1;
  j1939_id |= data_page_bit_; //Set the data page bit
  j1939_id <<= 8;
  j1939_id |= pdu_format_; //set pdu format byte
  j1939_id <<= 8;
  j1939_id |= pdu_specifier_; //set pdu specifer byte
  j1939_id <<= 8;
  j1939_id |= source_address_; //set source address byte

  return j1939_id;
}

uint32_t linsock::can::j1939::Frame::GetPGN() const
{
  uint32_t pgn{};
  pgn |= data_page_bit_;
  pgn <<= 8;
  pgn |= pdu_format_;
  pgn <<= 8;
  pgn |= pdu_specifier_;
  return pgn;
}

can_frame linsock::can::j1939::Frame::ToCanFrame() const
{
  ///TODO: Check data size or assume that it will allways be bellow 8
  ///NOTE: !!! Need to initialize can framw with the reserved values !!!
  ///If not other controller applications will not accept the frames
  ///even though they look the same in the candump program
  can_frame frame{GetHeader(), static_cast<unsigned char>(data_.size()),0,0,0,{}};
  for(uint8_t i = 0; i < frame.can_dlc; i++)
  {
    frame.data[i] = data_[i];
  }
  return frame;
}