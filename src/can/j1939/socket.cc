#include "linsock/can/j1939/socket.h"

#include <fstream>
#include <thread>
#include <chrono>
#include <stdexcept>


linsock::can::j1939::Socket::Socket(const int fd, 
  std::shared_ptr<Network> network, const uint64_t bound_name ) :
  can_socket_(fd), ctrl_network_(network), bound_name_(bound_name),
  connection_name_(std::nullopt)
{
  if(ctrl_network_ == nullptr)
  {
    throw std::invalid_argument("Not a broadcast frame");
  }
}

linsock::can::j1939::Socket::Socket(const int fd, std::shared_ptr<Network> network,
const uint64_t bound_name, const uint64_t connected_name ) :
 can_socket_(fd), ctrl_network_(network), 
 bound_name_(bound_name), connection_name_(connected_name)
{
  assert(network);
}

ssize_t linsock::can::j1939::Socket::WriteRaw(const Frame& j1939_frame)
{
  return can_socket_.Write(j1939_frame.ToCanFrame()); //Mutex locked;
}

ssize_t linsock::can::j1939::Socket::WriteBroadcast(Frame& j1939_frame)
{
  if(!j1939_frame.IsBroadCast())
  {
    throw std::invalid_argument("Not a broadcast frame");
  }

  auto source_address = ctrl_network_->GetAddress(bound_name_);
  if(source_address == ADDRESS_NULL)
  {
    throw std::invalid_argument("Socket has no source address");
  }

  j1939_frame.SetSourceAdderess(source_address);
  return WriteRaw(j1939_frame);
}


ssize_t linsock::can::j1939::Socket::Write(Frame& j1939_frame)
{
  if(!connection_name_.has_value())
  {
    throw std::invalid_argument("Socket has no connected controller app");
  }

  return WriteTo(*connection_name_, j1939_frame);
}


ssize_t linsock::can::j1939::Socket::WriteTo(const uint64_t destination, Frame& j1939_frame)
{
  auto source_address = ctrl_network_->GetAddress(bound_name_);
  if(source_address == ADDRESS_NULL)
  {
    throw std::invalid_argument("Socket has no source address");
  }

  auto destination_address = ctrl_network_->GetAddress(destination);
  if(source_address == ADDRESS_NULL)
  {
    throw std::invalid_argument("Destination has no address");
  }

  j1939_frame.SetSourceAdderess(source_address);
  j1939_frame.SetPDUSpecifier(destination_address);

  return WriteRaw(j1939_frame);
}

std::tuple<ssize_t, linsock::can::j1939::Frame> linsock::can::j1939::Socket::Read()
{
  auto[read_bytes, frame] = can_socket_.Read();
  return {read_bytes, {frame}};
}

uint8_t linsock::can::j1939::Socket::Bind(const uint8_t preffered_address, bool force)
{
  if(ctrl_network_->InNetwork(bound_name_))
  {
    //Already claimed, or claiming address
    return ctrl_network_->GetAddress(bound_name_);
  }

  auto address = ctrl_network_->FindAddress(bound_name_, preffered_address, force);
  if(address >= ADDRESS_NULL)
  {
    return ADDRESS_NULL;
  }

  ///TODO: Should it register in the network before or after, in case of loopback or read own frames
  ///As we need to use bind to indicate that the device is local or not?
  ///TODO: Yeah need to check if can frame is set to read own frames, cuse if it does the claim is not needed

  if(!ctrl_network_->Insert(bound_name_, address))
  {
    ///TODO: Error,failed to claim address
    return ADDRESS_NULL;
  }

  auto bytes = can_socket_.Write(
    Frame::AddressClaimFrame(bound_name_, address).ToCanFrame());
  if(bytes < 0)
  {
    ///TODO: Error
    return ADDRESS_NULL;
  }

  return address;
}

bool linsock::can::j1939::Socket::Connect(const uint64_t name)
{
  if(ctrl_network_->InNetwork(name))
  {
    connection_name_ = name;
    return true;
  }

  return false;
}

int linsock::can::j1939::Socket::Poll(const int number, const int time) const
{
  return can_socket_.Poll(number, time);
}

bool linsock::can::j1939::Socket::AddFilter(const std::vector<can_filter> filters)
{
  return can_socket_.AddFilter(filters);
}