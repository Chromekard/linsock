
# LinJ1939

*Linux C++ 17 J1939 library*

Notes:
Since the linux/j1939.h drives and protocol do not expose the underlying name - SA register. It is basicly required by the user
to maintain their own. As is done in canutils/j1939acd.h (address claim deamon). Personaly i find this quite annoying. In any case
since this implementation does not use linux/j1939.h yet, to do good address claiming it found that i would either have to make a deamon or
a background thread thats filtered to only take address related frames. Another alternative would be to implement this in each socket, but
then the issue is that sockets on the same device using the same name might all respond to address claims. One solution to this would be to only
have one of the sockets get address related frames. But feels like that would quickly become a mess if that socket was closed and then another socket would
have to take up the task all of a sudden.

Also in the case that linux/j1939.h is implemented i still feel like a address claiming deamon or the like would still be needed because we still dont have access
to the map. See (https://github.com/linux-can/can-utils/issues/282). So yeah back to square one on that one util linux/j1939.h is updated to allow map access.
Might just have to implement it myself if it comes to it. Anyway this is just me grumbling to justify my design choices so whatever.

So currently best way to do address claiming is to in each of your controller application classes that i asume have a j1939 socket member would be to read
frames and the pass address reltaed frames to the shared network, and also just make sure not to send in things multiple times if using multiple connections on the same
name. The network will handle address claims, but on conflic with local devices and address request and such you are on your own.

Or use the background thread that i will implement.

----
##Features