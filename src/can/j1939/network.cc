#include "linsock/can/j1939/network.h"

bool linsock::can::j1939::Network::Insert(const uint64_t ctrl_name, const uint8_t address)
{
  std::scoped_lock lock{network_mtx_};

  auto it = addr_ctrl_map_.find(address);
  if(it != addr_ctrl_map_.end())
  {
    //Their name is less than ours cant claim
    if(it->second < ctrl_name)
    {
      return false;
    }
    ///TODO: Move existing name to null address list.
  }

  addr_ctrl_map_[address] = ctrl_name;
  ctrl_addr_map_[ctrl_name] = address;
  return true;
}

bool linsock::can::j1939::Network::Available(const uint8_t address) const
{
  std::shared_lock lock{network_mtx_};
  return addr_ctrl_map_.find(address) != addr_ctrl_map_.end();
}

bool linsock::can::j1939::Network::InNetwork(const uint64_t ctrl_name) const
{
  std::shared_lock lock{network_mtx_};
  return ctrl_addr_map_.find(ctrl_name) != ctrl_addr_map_.end();
}


uint8_t linsock::can::j1939::Network::GetAddress(const uint64_t ctrl_name) const
{
  std::shared_lock lock{network_mtx_};
  auto it = ctrl_addr_map_.find(ctrl_name);
  if(it == ctrl_addr_map_.end())
  {
    return ADDRESS_NULL;
  }

  return it->second;
}

uint8_t linsock::can::j1939::Network::FindAddress(const uint64_t name, uint8_t preffed_address, bool force) const
{
  std::shared_lock lock{network_mtx_};

  //If nothing in map return address
  for(uint8_t address = preffed_address; address < j1939::ADDRESS_NULL; address++)
  {
    auto pair = addr_ctrl_map_.find(address);
    if(pair == addr_ctrl_map_.end())
    {
      return address;
    }

    //Claim address is we have smaller name
    if(pair->second > name && force)
    {
      return address;
    }
  }

  //if no address was found above the preffered address, check bellow
  for(uint8_t address = 0; address < preffed_address; address++)
  {
    auto pair = addr_ctrl_map_.find(address);
    if(pair == addr_ctrl_map_.end())
    {
      return address;
    }

    //Claim address is we have smaller name
    if(pair->second > name && force)
    {
      return address;
    }
  }

  return j1939::ADDRESS_NULL;
}

/*
std::optional<uint64_t> linsock::can::j1939::Network::ClaimAddress(const uint64_t name, const uint8_t address)
{
  bool exists{};
  std::optional<uint64_t> conflict{std::nullopt};
  {
    std::shared_lock lock{network_mtx_};
    //Check if the controller is already in the map
    exists = addressed_controllers_.find(name) != addressed_controllers_.end();

    //Check if another controller claims that address
    for(auto pair : addressed_controllers_)
    {
      if(pair.second == address)
      {
        if(pair.first == name)
        {
          //This controller has already claimed this address
          return std::nullopt;
        }
        conflict = pair.first;
        break;
      }
    }
  }

  if(!conflict)
  {
    if(!exists)
    {
      {
        //No conflic & the device did not exist, add as a new device
        std::scoped_lock lock{network_mtx_};
        addressed_controllers_[name] = address;
      }
      if(OnNewControllerAppCallback)
      {
        OnNewControllerAppCallback(name, address);
      }
    }
    else
    {
      uint8_t old_address{ADDRESS_NULL};
      {
        //No conflick, but device already exists. Will change the address
        std::scoped_lock lock{network_mtx_};
        old_address = addressed_controllers_[name];
        addressed_controllers_[name] = address;
      }
      if(OnAddressChangedCallback)
      {
        OnAddressChangedCallback(name, old_address, address);
      }
    }
    return std::nullopt;
  }

  //Address with the lowest name has priority
  auto conflicting_name = conflict.value();
  if(conflicting_name > name)
  {
    {
      std::scoped_lock lock{network_mtx_};
      addressed_controllers_[conflicting_name] = j1939::ADDRESS_NULL;
    }

    //Conflicking address losing its address
    if(OnAddressChangedCallback)
    {
      OnAddressChangedCallback(conflicting_name, address, j1939::ADDRESS_NULL);
    }

    if(!exists)
    {
      {
        //Add as a new device
        std::scoped_lock lock{network_mtx_};
        addressed_controllers_[name] = address;
      }
      if(OnNewControllerAppCallback)
      {
        OnNewControllerAppCallback(name, address);
      }
    }
    else
    {
      uint8_t old_address{ADDRESS_NULL};
      {
        //Device already exists. Will change the address
        std::scoped_lock lock{network_mtx_};
        old_address = addressed_controllers_[name];
        addressed_controllers_[name] = address;
      }
      if(OnAddressChangedCallback)
      {
        OnAddressChangedCallback(name, old_address, address);
      }
    }

    return conflicting_name;
  }

  //Could not claim the address
  return name;
}
*/


///TODO: Place the following into the deamon/ background thread/ own application

/*
bool linsock::can::j1939::Network::DiscoverNetwork()
{
  auto ret = linsock::can::Socket::Write(Frame::AddressRequestFrame().ToCanFrame()) > 0;
  std::this_thread::sleep_for(std::chrono::milliseconds(250));

  ///TODO: Add check for how long ago last claim was recieved
  auto start_time = std::chrono::steady_clock::now();
  while((std::chrono::steady_clock::now() - start_time) < std::chrono::seconds(2))
  {
    ReadFrame();
  }
  return ret;
}

uint8_t linsock::can::j1939::Network::Bind(const uint64_t name, const uint8_t preffered_address)
{
  auto address = ctrl_network_->RegisterCtrlApp(name, preffered_address);
  if(address >= ADDRESS_NULL)
  {
    return ADDRESS_NULL;
  }

  {
    std::scoped_lock lock{network_mtx_};
    addressed_controllers_[name_] = address;
  }

  auto bytes = linsock::can::Socket::Write(
    Frame::AddressClaimFrame(name_, address).ToCanFrame());
  if(bytes < 0)
  {
    ///TODO: Error
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(250));

  ///TODO: At this point we should loop read for counter address claims

  ///TODO: Add check for how long ago last claim was recieved
  auto start_time = std::chrono::steady_clock::now();
  while((std::chrono::steady_clock::now() - start_time) < std::chrono::seconds(2))
  {
    ReadFrame();
  }

  return address;
}

linsock::can::j1939::Frame linsock::can::j1939::Network::ReadFrame()
{
  auto[read_bytes, frame] = can_socket_.Read();
  if(read_bytes < 0)
  {
    ///TODO: Error
    ///TODO: Return bool on error?
    return {};
  }

  Frame j1939_frame{frame};
  
  auto PGN = j1939_frame.GetPGN();
  if(PGN == PGN_REQUEST_ADDRESS)
  {
    auto opt_frame = ctrl_network_->HandleAddressRequest();
    if(opt_frame)
    {
      auto write_bytes = can_socket_.Write(opt_frame.value().ToCanFrame()); //Mutex locked
      if(write_bytes < 0)
      {
        ///TODO: Error
      }
    }

    ///TODO: Return bool on error?
    return j1939_frame;
  }

  if(PGN == PGN_ADDRESS_CLAIM)
  {
    auto opt_name = ctrl_network_->HandleAddressClaims(
      Frame::J1939DataToUint64(j1939_frame.GetData()),
      j1939_frame.GetSourceAdderess());
    
    ///TODO: Should allways send address claim on conflict

    if(!opt_name.has_value())
    {
      return std::nullopt;
    }

    if(opt_name.value() != bound_name_)
    {
      return std::nullopt;
    }

    auto address = ctrl_network_->FindAddress();
    if(address == ADDRESS_NULL)
    {
      if(ctrl_network_->OnNetworkFullCallback)
      {
        ctrl_network_->OnNetworkFullCallback();
      }
      return std::nullopt;
    }

    auto write_bytes = can_socket_.Write(
      Frame::AddressClaimFrame(opt_name.value(), address).ToCanFrame()); //Mutex locked
    if(write_bytes < 0)
    {
      ///TODO: Error
    }
  }

  return j1939_frame;
}

*/