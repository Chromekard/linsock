#include "linsock/can/socket.h"

#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <string.h>
//#include <sys/types.h>
//#include <sys/socket.h>
#include <linux/can/raw.h>

std::tuple<ssize_t, can_frame> linsock::can::Socket::Read()
{
  can_frame frame{};
  auto nbytes = ::read(fd_, &frame, sizeof(can_frame));
  return {nbytes, frame};
}

ssize_t linsock::can::Socket::Write(can_frame frame)
{
  std::scoped_lock<std::mutex> lock(socket_mtx_);
  if(frame.can_dlc > mtu_)
  {
    return -1;
  }

  return ::write(fd_, &frame, sizeof(frame));
}

bool linsock::can::Socket::AddFilter(const std::vector<can_filter> filters)
{
  ///TODO: Check that this is correct
  if(setsockopt(fd_, SOL_CAN_RAW, CAN_RAW_FILTER, filters.data(), sizeof(filters)) < 0)
  {
    return false;
  }

  return true;
}