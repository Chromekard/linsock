#include "linsock/can/socket_builder.h"

#include <iostream>

#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <string.h>
//#include <sys/types.h>
//#include <sys/socket.h>
#include <linux/can/raw.h>

namespace
{

}

int linsock::can::SocketBuilder::Build(int flags)
{
  int socket = bcm_ ? ::socket(PF_CAN, SOCK_DGRAM | flags, CAN_BCM) : 
    ::socket(PF_CAN, SOCK_RAW | flags, CAN_RAW);
  if (socket < 0)
  {
    std::cout << "Failed to create socket" << std::endl;
    return errno;
  }

  ifreq ifr{};
  if(bind_all_)
  {
    ///TODO: Check if this works
	  ifr.ifr_ifindex = 0; 
  }
  else
  {
    //network_name_.copy(ifr.ifr_name, std::min(IFNAMSIZ - 1, static_cast<int>(network_name_.size()) - 1));
    ///TODO: Figure out if 0 terminator is needed
    ::strncpy(ifr.ifr_name, network_name_.c_str(), IFNAMSIZ - 1);
    ifr.ifr_name[IFNAMSIZ - 1] = '\0';
	  ifr.ifr_ifindex = if_nametoindex(ifr.ifr_name);
  }

	sockaddr_can addr{};
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

  int bound = bcm_ ? ::connect(socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) :
     ::bind(socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr));
  if (bound < 0)
  {
    std::cout << "Failed to bind/connect socket" << std::endl;
    return -2;
  }

  ///NOTE: If done before bind, the bind function will fail as ifr_ifindex is changed to the same value as ifr_mtu
  if (ioctl(socket, SIOCGIFMTU, &ifr) < 0) 
  {
    std::cout << "Failed to read mtu size" << std::endl;
    return -3;
  }

  mtu_ = ifr.ifr_mtu;
  return socket;
}