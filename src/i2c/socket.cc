#include "linsock/i2c/socket.h"

#include <string>

#include <sys/ioctl.h>

namespace
{
/*
  bool I2C_Message(int fd, i2c_rdwr_ioctl_data& data)
  {
    return !(ioctl(fd, I2C_RDWR, &data) < 0);
  }

  bool I2C_Message_Vector(int fd, std::vector<i2c_msg>& messages)
  {
    i2c_rdwr_ioctl_data data{messages.data(),messages.size()};
    return I2C_Message(fd, data);
  }
*/

  ///TODO: See about removing these
}

// ================================= SYSFS I/O ================================= //

bool linsock::i2c::Socket::IO_Write()
{
  return true;
}

bool linsock::i2c::Socket::IO_Read()
{
  return true;
}

// ================================= SMBUS ================================= //

bool linsock::i2c::Socket::SMBus_Send(const uint16_t address, i2c_smbus_ioctl_data& data, const bool force)
{
  std::scoped_lock<std::mutex> lock(socket_mtx_);
  if (ioctl(fd_, I2C_SLAVE, address) < 0)
  {
    if(!force)
    {
      return false;
    }

    if (ioctl(fd_, I2C_SLAVE_FORCE, address) < 0)
    {
      return false;
    }
  }

  if (ioctl(fd_, I2C_SMBUS, &data) < 0)
  {
    return false;
  }

  return true;
}


// ================================= I2C ================================= //

bool linsock::i2c::Socket::I2C_Write(const uint16_t address, const uint8_t register_address, uint8_t data)
{
  std::scoped_lock<std::mutex> lock(socket_mtx_);
  uint8_t output[2]{register_address, data};
  i2c_msg rdwr_msgs[1]{{address, 0, 2, output}};
  i2c_rdwr_ioctl_data message{rdwr_msgs,1};
  return !(ioctl(fd_, I2C_RDWR, &message) < 0);
}

bool linsock::i2c::Socket::I2C_Write(const uint16_t address, const uint8_t register_address, const std::vector<uint8_t> data)
{
  std::scoped_lock<std::mutex> lock(socket_mtx_);
  std::vector<uint8_t>tmp{register_address}; //Add register address to the front
  tmp.insert(std::end(data), std::begin(data), std::end(data));
  i2c_msg rdwr_msgs[1]{{address, 0, tmp.size(), tmp.data()}};
  i2c_rdwr_ioctl_data message{rdwr_msgs,1};
  return !(ioctl(fd_, I2C_RDWR, &message) < 0);
}

bool linsock::i2c::Socket::I2C_Read(const uint16_t address, uint8_t register_address, uint8_t& data)
{
  std::scoped_lock<std::mutex> lock(socket_mtx_);
  i2c_msg rdwr_msgs[2]{
    {address, 0, 1, &register_address},
    {address, I2C_M_RD, 1, &data}};
  i2c_rdwr_ioctl_data message{rdwr_msgs, 2};
  return !(ioctl(fd_, I2C_RDWR, &message) < 0);
}

bool linsock::i2c::Socket::I2C_Read(const uint16_t address, uint8_t register_address, std::vector<uint8_t> &data)
{
  std::scoped_lock<std::mutex> lock(socket_mtx_);
  i2c_msg rdwr_msgs[2]{
    {address, 0, 1, &register_address},
    {address, I2C_M_RD, data.size(), data.data()}};
  i2c_rdwr_ioctl_data message{rdwr_msgs, 2};
  return !(ioctl(fd_, I2C_RDWR, &message) < 0);
}