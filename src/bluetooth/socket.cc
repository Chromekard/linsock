#include "linsock/bluetooth/socket.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>

std::shared_ptr<linsock::bluetooth::Client> linsock::bluetooth::Socket::Listen() const
{
  // accept one connection
  sockaddr_rc connection{};
  socklen_t opt = sizeof(connection);
  auto client_ = ::accept(fd_, reinterpret_cast<sockaddr*>(&connection), &opt);
  if (client_ < 0)
  {
    return nullptr;
  }

  //Print address
  //::ba2str(&remoteAddr_.rc_bdaddr, &buffer_[0]);
  
  return std::make_shared<Client>(client_, connection);
}


int linsock::bluetooth::Socket::Close()
{
  return linsock::Socket::Close();

  ///TODO: Close sdp session

  ///TODO: Not closing could be causing a memory leak
  //Commented this code out as it was causing segmentation faults.
  //Tried implementing the code in source for further debugging
  //found that freeing session is causes invalid pointer error
  //So maybe that was the issue?
  //if(::sdp_close(session_) < 0)
  //{
  //}
}