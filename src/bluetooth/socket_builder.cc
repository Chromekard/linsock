#include "linsock/bluetooth/socket_builder.h"

std::tuple<int, sockaddr_rc, sdp_session_t*> linsock::bluetooth::SocketBuilder::Build(int flag)
{
  sockaddr_rc localAddr_{};
  localAddr_.rc_family = AF_BLUETOOTH;
  localAddr_.rc_bdaddr = bd_address_;
  localAddr_.rc_channel = channel_;

  sdp_session_t* session_ = RegisterService();
  if(!session_)
  {
    if(OnError) {OnError("Failed to register sdp session");}
    return {-1, localAddr_, session_};
  }

  int socket = ::socket(AF_BLUETOOTH, SOCK_STREAM | flag, BTPROTO_RFCOMM);
  if (socket < 0)
  {
    if(OnError) {OnError("Failed to create socket endpoint");}
    return {socket, localAddr_, session_};
  }

  //Note: Could remove the reienterpret cast by manually adding data 
  //from local socket type to a sockaddr struct

  if (::bind(socket, reinterpret_cast<sockaddr*>(&localAddr_), sizeof(localAddr_)) < 0)
  {
    if(OnError) {OnError("Failed to bind socket endpoint");}
    return {socket, localAddr_, session_};
  }

  //Set too listening mode and the amount of possible active connections
  if (::listen(socket, max_connections_) < 0)
  {
    if(OnError) {OnError("Failed to set socket to listening mode");}
    return {socket, localAddr_, session_};
  }

  return {socket, localAddr_, session_};
}

sdp_session_t *linsock::bluetooth::SocketBuilder::RegisterService()
{
  ///TODO: Make a parameter
  uint32_t svc_uuid_int[4] = {0x01110000, 0x00100000, 0x80000080, 0xFB349B5F}; //a1c57e78-f88c-11e8-8eb2-f2801f1b9fd1

  // creat and set the general service ID
  uuid_t svc_uuid;
  sdp_record_t *record = sdp_record_alloc();
  if(!record)
  {
    if(OnError) {OnError("Failed to allocate sdp record");}
    return nullptr;
  }
  sdp_uuid128_create(&svc_uuid, &svc_uuid_int);
  sdp_set_service_id(record, svc_uuid);

  //Print uuid to text
  //char str[256] = "";
  //sdp_uuid2strn(&svc_uuid, str, 256);

  // set the service class
  uuid_t svc_class_uuid;
  sdp_uuid16_create(&svc_class_uuid, SERIAL_PORT_SVCLASS_ID);
  sdp_list_t *svc_class_list = sdp_list_append(0, &svc_class_uuid);
  if(!svc_class_list)
  {
    if(OnError) {OnError("Failed to get class list");}
    return nullptr;
  }
  sdp_set_service_classes(record, svc_class_list);

  // set the Bluetooth profile information
  sdp_profile_desc_t profile;
  sdp_uuid16_create(&profile.uuid, SERIAL_PORT_PROFILE_ID);
  profile.version = 0x0100;
  sdp_list_t *profile_list = sdp_list_append(0, &profile);
  if(!profile_list)
  {
    if(OnError) {OnError("Failed to get profile list");}
    return nullptr;
  }
  sdp_set_profile_descs(record, profile_list);

  // make the service record publicly browsable
  uuid_t root_uuid;
  sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
  sdp_list_t *root_list = sdp_list_append(0, &root_uuid);
  if(!root_list)
  {
    if(OnError) {OnError("Failed to get root list");}
    return nullptr;
  }
  sdp_set_browse_groups(record, root_list);

  // set l2cap information
  uuid_t l2cap_uuid;
  sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
  sdp_list_t *l2cap_list = sdp_list_append(0, &l2cap_uuid);
  if(!l2cap_list)
  {
    if(OnError) {OnError("Failed to get l2cap list");}
    return nullptr;
  }
  sdp_list_t *proto_list = sdp_list_append(0, l2cap_list);
  if(!proto_list)
  {
    if(OnError) {OnError("Failed to get proto list");}
    return nullptr;
  }

  // register the RFCOMM channel for RFCOMM sockets
  uuid_t rfcomm_uuid;
  sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
  sdp_data_t *channel = sdp_data_alloc(SDP_UINT8, &channel_);
  if(!channel)
  {
    if(OnError) {OnError("Failed to allocate channel data");}
    return nullptr;
  }
  sdp_list_t *rfcomm_list = sdp_list_append(0, &rfcomm_uuid);
  if(!rfcomm_list)
  {
    if(OnError) {OnError("Failed to get rfcomm list");}
    return nullptr;
  }
  sdp_list_append(rfcomm_list, channel);
  sdp_list_append(proto_list, rfcomm_list);

  sdp_list_t *access_proto_list = sdp_list_append(0, proto_list);
  if(!access_proto_list)
  {
    if(OnError) {OnError("Failed to get access protocol list");}
    return nullptr;
  }
  sdp_set_access_protos(record, access_proto_list);

  // set the name, provider, and description
  sdp_set_info_attr(record, sdp_service_name_.c_str(), 
    sdp_service_provider_.c_str(), 
    sdp_service_description_.c_str());

  /// connect to the local SDP server, NOTE: requires sudo to work
  sdp_session_t *session = sdp_connect(&bd_address_, 
    &sdp_device_address_, SDP_RETRY_IF_BUSY);
  if(!session)
  {
    if(OnError) {OnError("Failed to connect to local SDP server");}
    return nullptr;
  }

  //Register record
  int err = sdp_record_register(session, record, 0);
  if (err < 0)
  {
    if(OnError) {OnError("Failed to register spd record, error number " + std::to_string(err));}
  }

  // cleanup
  sdp_data_free(channel);
  sdp_list_free(l2cap_list, 0);
  sdp_list_free(rfcomm_list, 0);
  sdp_list_free(root_list, 0);
  sdp_list_free(access_proto_list, 0);
  sdp_list_free(svc_class_list, 0);
  sdp_list_free(profile_list, 0);

  return session;
}