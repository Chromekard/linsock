#include "linsock/uart/socket_builder.h"

///Linux headers
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include <chrono>
#include <thread>

///TODO: Some way to determine the board being used

namespace
{
  //Nano Devkit
  //const std::string DEVICE_TTY = "/dev/ttyTHS1";
  // /dev/ttyS0 = UART0 Jetson Quasar
  // /dev/ttyTHS2 = UART1 Jetson Quasar
  //Maybe use /serial0 if that works?
}

int linsock::uart::SocketBuilder::Build(int flags)
{

	//  O_NDELAY / O_NONBLOCK (same function) 
  //    - Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
  //      if there is no input immediately available (instead of blocking). Likewise, write requests can also return
	//      immediately with a failure status if the output can't be written immediately.
  //      Caution: VMIN and VTIME flags are ignored if O_NONBLOCK flag is set.

	//  O_NOCTTY 
  //    - When set and path identifies a terminal device open() shall not cause the terminal device to become 
  //      the controlling terminal for the process.fid = open("/dev/ttyTHS1", O_RDWR | O_NOCTTY | O_NDELAY);

  //Open in non blocking read/write mode
  int fd_ = ::open(tty_path_.c_str(), flags); //O_RDWR | O_NOCTTY
  if (fd_ < 0 || !::isatty(fd_)) 
  {
    return fd_;
  }

  //Open device in none blocking mode.
  ///TODO: look more into this
  //fcntl(fd_, F_SETFL, FNDELAY);

  ::tcflush(fd_, TCIFLUSH);
  ::tcflush(fd_, TCIOFLUSH);

    //Wait before flushing the file descriptor, might not be needed.
  std::this_thread::sleep_for(std::chrono::seconds(1));

  //Get terminal attributes
  termios tty{};
  if(::tcgetattr(fd_, &tty) != 0) 
  {
    return -2;
  }

  ///TODO: Make these into options that can be set

  //::cfmakeraw(&tty);

  //Change attributes
  tty.c_cflag &= ~PARENB;               // Clear parity bit, disabling parity (most common)
  tty.c_cflag &= ~CSTOPB;               // Clear stop field, only one stop bit used in communication (most common)
  tty.c_cflag &= ~CRTSCTS;              // Disable RTS/CTS hardware flow control (most common)
  tty.c_cflag &= ~CSIZE;               // Clear all the size bits
  tty.c_cflag |= CREAD | CLOCAL | CS8;  // Turn on READ, ignore ctrl lines (CLOCAL = 1), set 8 bits per byte

  tty.c_lflag &= ~ICANON; //Disable Canonical mode (ie line by line data)
  tty.c_lflag &= ~ECHO;   // Disable echo
  tty.c_lflag &= ~ECHOE;  // Disable erasure
  tty.c_lflag &= ~ECHONL; // Disable new-line echo
  tty.c_lflag &= ~ISIG;   // Disable interpretation of INTR, QUIT and SUSP
  //tty.c_lflag = 0;          //  enable raw input instead of canonical,


  tty.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  // Non Cannonical mode
  tty.c_iflag &= ~(IXON | IXOFF | IXANY );                           // Turn off s/w flow ctrl
  tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received byte

  tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
  tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed

  //No blocking, return immediately with what is available
  tty.c_cc[VMIN] = 1;  //Wait for x amount of bytes to be available. Max 255 bytes (characters)
  tty.c_cc[VTIME] = 0; //Wait for x amount of seconds before timeout. Time in 1/10 seconds to wait (max 25.5s)

  ::cfsetispeed(&tty, B9600); //Set input speed
  ::cfsetospeed(&tty, B9600); //Set output speed

  //Apply attributes
  if (::tcsetattr(fd_, TCSANOW, &tty) != 0) 
  {
    return -2;
  }

  ::tcflush(fd_, TCIFLUSH);
  ::tcflush(fd_, TCIOFLUSH);

  //Wait before flushing the file descriptor, might not be needed.
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  return fd_;
}