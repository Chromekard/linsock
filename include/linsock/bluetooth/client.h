#pragma once

#include "bluetooth/bluetooth.h"
#include "bluetooth/rfcomm.h"

#include "../raw_socket.h"

namespace linsock::bluetooth
{

/**
 * Bluetooth client class, used to read and write data to the specific client
*/
class Client : public RawSocket
{
public:

  /**
   * Constuctor
   * @param fd used to comminicate over
   * @param address of the client
  */
  Client(const int fd, const sockaddr_rc address) : RawSocket(fd), address_(address){}

  /**
   * Set the address of the client
   * @param address to change too
  */
  void SetAddress(const sockaddr_rc address)
  {
    address_ = address;
  }

  ///TODO: Add own read write functions or inherite from raw socket

private:
  sockaddr_rc address_{};
};

} // namespace cppblue