#pragma once

#include <string>
#include <functional>
#include <tuple>

#include "bluetooth/bluetooth.h"
#include "bluetooth/rfcomm.h"
#include "bluetooth/sdp.h"
#include "bluetooth/sdp_lib.h"

namespace linsock::bluetooth
{

/**
 * 
*/
class SocketBuilder
{
public:

  /**
   * @param bd_address address to local bluetooth device, use str2ba to convert sting to address
   * example use address "00:00:00:00:00:00" to select any available local bluetooth device
   * or use bracket initialization {{0,0,0,0,0,0}}
   * @return reference to self
  */
  SocketBuilder& SetAdress(const bdaddr_t bd_address)
  {
    bd_address_ = bd_address;
    return *this;
  }

  /**
   * @param channel to bind the device to
   * @return reference to self
   */
  SocketBuilder& SetChannel(const uint8_t channel)
  {
    channel_ = channel;
    return *this;
  }

  /**
   * @param max_connections to accept
   * @return reference to self
   */
  SocketBuilder& SetMaxConnections(const int max_connections)
  {
    max_connections_ = max_connections;
    return *this;
  }

  /**
   * @param sdp_device_address to the service discovery server
   * @return reference to self
   */
  SocketBuilder& SetSdpAddress(const bdaddr_t sdp_device_address)
  {
    sdp_device_address_ = sdp_device_address;
    return *this;
  }

  /**
   * @param sdp_service_name service name
   * @return reference to self
   */
  SocketBuilder& SetSdpServiceName(const std::string sdp_service_name)
  {
    sdp_service_name_ = sdp_service_name;
    return *this;
  }

  /**
   * @param sdp_service_description description of the service
   * @return reference to self
   */
  SocketBuilder& SetSdpServiceDescription(const std::string sdp_service_description)
  {
    sdp_service_description_ = sdp_service_description;
    return *this;
  }

  /**
   * @param sdp_service_provider provider of the service
   * @return reference to self
   */
  SocketBuilder& SetSdpServiceProvider(const std::string sdp_service_provider)
  {
    sdp_service_provider_ = sdp_service_provider;
    return *this;
  }

  /**
   * Preform any instructions and setup of bluetooth requirements needed to open a bluetooth socket
   * @param flag for the socket, use it to set SOCK_NONBLOCK or SOCK_CLOEXEC, defaults to 0 for no flags
   * @return file descriptor, socket address structure, sdp session connection
  */
  std::tuple<int, sockaddr_rc, sdp_session_t*> Build(int flag);


  /**
   * Function for passing specific error text to the user for debugging
   * In addition to using the string returen from this function also check
   * strerror(errno) for more information about the error
  */
  std::function<void(std::string)> OnError{};

private:
  
  /**
   * SDP service registration function
  */
  sdp_session_t *RegisterService();

  /// Variables ///

  bdaddr_t bd_address_{{0,0,0,0,0,0}};
  uint8_t channel_{};
  int max_connections_{1};
  bdaddr_t sdp_device_address_{};
  std::string sdp_service_name_{}; 
  std::string sdp_service_description_{};
  std::string sdp_service_provider_{};
};

}