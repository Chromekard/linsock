#pragma once

#include <memory>

#include "bluetooth/bluetooth.h"
#include "bluetooth/rfcomm.h"
#include "bluetooth/sdp.h"
#include "bluetooth/sdp_lib.h"

#include "../socket.h"
#include "client.h"

namespace linsock::bluetooth
{

/**
 * Bluetooth socket class that wraps around bluez code.
 * This socket is used to listen for incomming connection
 * This class might require to be run using admin privliges as
 * the sdp service might require it to run propperly.
*/
class Socket : public linsock::Socket
{
public:
  
  /**
  * Constructor
  * @param fd is the file desciptor of the bluetooth socket
  * @param address that the socket is using
  * @param sdp_session that is used to talk with sdp service
  */
  Socket(const int fd, const sockaddr_rc address, const sdp_session_t* sdp_session) : 
    linsock::Socket(fd), localAddr_(address), sdp_session_(sdp_session)
  {}

  /**
   * 
  */
  int Close() override;

  /**
  * Listen for connection.
  * @return True if a connection was made, false if the connenction failed. Also returnes
  * instance of the connected client
  */
  std::shared_ptr<Client> Listen() const;

  const sockaddr_rc GetLocalAddress() const
  {
    return localAddr_;
  }

  ///TODO: Maybe listen should not returned a shared pointer but maybe a rvalue, std::move()

private:

  const sockaddr_rc localAddr_{};
  const sdp_session_t* sdp_session_{nullptr};
};

} // namespace cppblue