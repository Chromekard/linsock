#pragma once

//Standard headers
#include <mutex>
#include <functional>
#include <string>
#include <stdexcept>

//Linux headers
#include <unistd.h>
#include <fcntl.h>

namespace linsock
{

/**
 * Base socket for linux io
*/
class Socket
{
public:

  ///Dont allow copy as it would negate mutlithreading safety
  ///Need to use pointers or references to share instance between threads

  Socket(const Socket &) = delete;
  Socket &operator=(const Socket &) = delete;

  //Might be fine to allow move while mutex is mutable
  //Socket(Socket &&) = delete;
  //Socket &operator=(Socket &&) = delete;

  /**
   * Destructor
  */
  virtual ~Socket()
  {
    Close();
  }

  /**
   * Check if the socket is open
  */
  inline bool IsOpen() const
  {
    return ::fcntl(fd_, F_GETFL) > -1;
  }

  /**
   * Close the socket if its open
  */
  inline virtual int Close()
  {
    std::scoped_lock<std::mutex> lock(socket_mtx_);
    if(IsOpen())
    {
      return ::close(fd_);
    }
    return 0;
  }

protected:

  /**
   * Protected constructor, as we dont want any classes of this type to be created
   * @param file_descriptor for the socket
  */
  Socket(const int fd) : fd_(fd)
  {
    if(fd < 0)
    {
      throw std::invalid_argument("");
    }
  }

  const int fd_{-1};
  mutable std::mutex socket_mtx_{}; //Needed?
};

}