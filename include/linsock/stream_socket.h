#pragma once

#include <poll.h>

#include "socket.h"

namespace linsock
{

/**
 * Base socket for stream socket types, sockets that inheret from this class will be able to use poll
*/
class StreamSocket : public Socket
{
public:

  /**
   * Wrapper for poll.h poll function the socket to check if there is any data
   * @param number of data events to wait for
   * @param time to wait before interupt, in ms, 0 to return emidiatly, negative value to wait indefnifitly
   * @return Returns the number of file descriptors with events, zero if timed out,
   * or -1 for errors.
  */
  inline int Poll(const int number, const int time = 500) const
  {
    //Does not seem like a lock is needed as poll will wait for fd to become available
    pollfd poll_fd{fd_, POLLIN | POLLPRI, 0};
    return ::poll({&poll_fd}, number, time);
  }

protected:

  /**
   * Protected contsturctor to prevent creation of this type of class
   * other than from children
   * @param file_desciptor to read/write from
  */
  StreamSocket(const int fd) : Socket(fd){}

};

}