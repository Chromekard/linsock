#pragma once

#include <linux/can.h>

#include <tuple>
#include <vector>

#include "../stream_socket.h"

namespace linsock::can
{

/**
 * CAN socket for reading and writing data over can network
 * Currently does not support multi packet
*/
class Socket : public linsock::StreamSocket
{
public:

  /**
   * Constructor
  */
  Socket(const int fd) : linsock::StreamSocket(fd) {}

  /**
 * Writes a frame to the network
 * @param frame to write
 * @return true if successfull, false if not
 */
  virtual ssize_t Write(can_frame frame);

  /**
 * Reads a frame from the network
 * If not all the bytes are read a common error is errno 100, indicating that the network is down
 * which is not picked up by the open function
 * @return tuple containing the amount of bytes read and a can frame
 */
  virtual std::tuple<ssize_t, can_frame> Read();

  /**
 * Adds filters to the socket
 * @param filters to add
 * @return true if successfull, false if not
 */
  bool AddFilter(const std::vector<can_filter> filters);

protected:
  int mtu_{8};
};

} // namespace lincan