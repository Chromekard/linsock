#pragma once

#include <string>

namespace linsock::can
{

class SocketBuilder
{
public:

  ///TODO: Maybe add constructors?

  /**
   * Set the CAN protocol that the socket will use
   * @param bcm (broadcast manager) will be used if true, 
   * else RAW socket protocol will be used
   * @return reference to self
  */
  SocketBuilder& SetBCM(bool bcm)
  {
    bcm_ = bcm;
    return *this; 
  }

  /**
   * Set if the socket should bind to all available CAN interfaces
   * @param global bind if true, if false will check network name for binding
   * @return reference to self
   * @note is used recvfrom and sendto functions need to be used instead of
   * read and write, as which network needs to be secified
  */
  SocketBuilder& SetGlobalBind(bool global)
  {
    bind_all_ = global;
    return *this; 
  }

  /**
   * Set the network name that the socket will bind to
   * @param network_name to bind the socket to
   * @return reference to self
  */
  SocketBuilder& SetNetworkName(std::string network_name)
  {
    network_name_ = network_name;
    return *this; 
  }

  /**
   * @see ISocketBuilder
   * Build the socket based on configurations
   * @param flags to set on the socket when creating it. 
   * use it to set SOCK_NONBLOCK or SOCK_CLOEXEC, defaults to 0 for no flags TODO: Add a flag builder
   * @return socket file descriptor, < 0 on errors, 
   * -1 if create socket failed, -2 if bind failed 
   * user will be responsible for closing the socket
  */
  int Build(int flags = 0);

  /**
   * Returns how large the data of the can frames can be.
   * @return Normally 16 for legacy frames, but if CAN FD is supported it will be 72
  */
  int GetMaxTransferUnit()
  {
    return mtu_;
  }

private:

  //Inputs
  bool bcm_{};                        //Use broadcast manager instead of raw protocol
  bool bind_all_{};                   //Bind to all available can interfaces
  std::string network_name_{"can0"};  //Network to bind to example: can0, vcan0

  //Extra output
  int mtu_{16}; //IS 16 for legacy frames, is 72 if supports CAN FD frames
};

} // namespace linsock::can
