#pragma once

//C++
#include <cstdint>
#include <mutex>
#include <shared_mutex>
#include <unordered_map>
#include <set>
#include <optional>

//Local
#include "frame.h"

namespace linsock::can::j1939
{

/**
 * Storage class for maintaining the relation between
 * controller name and its address.
*/
class Network
{
public:

  Network() = default;

  ///TODO: Should be able to implement a copy, but deleted in the meantime
  Network(const Network &) = delete;
  Network &operator=(const Network &) = delete;

  //Move
  Network(Network &&) = default;
  Network &operator=(Network &&) = default;

  /// ##################### Copy Internal ##################### ///

  /**
   * Get a copy of the name address pairings in the internal map
   * @note depricated should not use
  */
  std::unordered_map<uint64_t, uint8_t> GetControllerMapCopy()
  {
    std::shared_lock lock{network_mtx_};
    return ctrl_addr_map_;
  }

  /**
   * 
  */
  std::set<uint64_t> GetControllerSet()
  {
    std::shared_lock lock{network_mtx_};
    std::set<uint64_t> set{ctrl_addr_map_.size()};
    for(auto&pair : ctrl_addr_map_)
    {
      set.insert(pair.first);
    }
    return set;
  }

  /// ##################### Map access ##################### ///

  /**
   * Add a controller app to the network, if already exists
   * the address will be changed
   * @param ctrl_name
   * @param address of the controller app
   * @return false if the address is already claimed by a lower name
  */
  bool Insert(const uint64_t ctrl_name, const uint8_t address);

  /**
   * Check if an address is taken
   * @param address to check
   * @return true if no controller has the address, false if a controller does
  */
  bool Available(const uint8_t address) const;

  /**
   * Check if a Controller application is registered in the network
   * @param ctrl_name to check for
   * @return true if in the network, false if not
  */
  bool InNetwork(const uint64_t ctrl_name) const;

  /**
   * Get the address of the application controller
   * @param ctrl_name that we want the address for
   * @return address, or ADDRESS_NULL if no address is claimed or not in the network
  */
  uint8_t GetAddress(const uint64_t ctrl_name) const;

  /**
   * Search the network empty addresses
   * @param name of the controller appliction looking for address
   * @param preffed_address to start search from
   * @param limit of the address search
   * @param force the taking of an address from another ecu
   * @return empty address, if no addresses were available NULL_ADDRESS is returned 
  */
  uint8_t FindAddress(const uint64_t name, const uint8_t preffed_address = 0, bool force = false) const;

private:

  ///TODO: Implement map as a bidirectional map, would need special container for null address
  std::unordered_map<uint64_t, uint8_t> ctrl_addr_map_{};
  std::unordered_map<uint8_t, uint64_t> addr_ctrl_map_{};
  
  mutable std::shared_mutex network_mtx_{};
};

}