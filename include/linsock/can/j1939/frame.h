#pragma once

#include <cstdint>
#include <vector>
#include <algorithm>

#include <linux/can.h>

#include "j1939.h"

namespace linsock::can::j1939
{

/**
 * Class that for manipulating a J1939 frame
 * 
 * Parameter Group Number (PGN) 
 * Protocol Data Unit (PDU)
 * 
 * Priority (Prio)
 * Reserved Bit (R)
 * Data Page (DP)
 * PDU format (PF)
 * PDU specific (PS)
 * Source Address (SA)
 * 
 * Example: 
 * (Tags):   --- 	Prio 	R 	DP 	PF 	      PS 	      SA
 * (Hex) :   0x0C 	            0xF0 	    0x04 	    0xEE
 * (Binary): 000 	011 	0 	0 	11110000 	00000100 	11101110
 * 
*/
class Frame
{
public:

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        Static Convertion                       @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Converts a unsinged 64 bit value into a vector of 8 bytes
   * @param data to convert to vector
   * @return vector containing 8 bytes
  */
  static std::vector<uint8_t> Uint64ToJ1939Data(const uint64_t data);

  /**
   * Converts a vector of max 8 bytes to unsigned 64 bit integer.
   * @note bytes will become {0,1 ... 7} turns into 76543210
   * @param data to convert
   * @return platform dependent unsigned 64 bit integer
  */
  static uint64_t J1939DataToUint64(const std::vector<uint8_t> data);

  /**
   * Converts a can_frame data array into a vector of bytes
   * @param array of can data
   * @param length of the array
   * @return vector of bytes containing array data
  */
  static std::vector<uint8_t> CanDataToByteVector(const unsigned char array[8], const uint8_t length);

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        Frame Archetypes                        @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Create an address request j1939 frame
   * Used to get the address of devices on the network
   * @return address request j1939 frame
  */
  static Frame AddressRequestFrame()
  {
    return Frame(static_cast<uint8_t>(6), false, PF_REQUEST, 
      ADDRESS_GLOBAL, ADDRESS_NULL, {0x00, 0xEE, 0x00});
  }
  
  /**
   * Create an address claim j1939 frame
   * @param name of the device
   * @param address to claim
   * @return address claim j1939 frame
  */
  static Frame AddressClaimFrame(const uint64_t name, const uint8_t address)
  {
    return Frame(static_cast<uint8_t>(6), false, PF_ADDRESS_CLAIM, 
      ADDRESS_GLOBAL, address, Uint64ToJ1939Data(name));
  }

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        Construction                            @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Default construction of empty frame
  */
  Frame(){};
  
  /**
  * Construct a j1939 using defined values
  * @param priority of the frame, is only 3 bits so can have values from 0...7
  * A value of 0 is highest prioritys.
  * @param data_page bit expands the PDU address by adding another bit of possible addresses
  * @param pdu_format (PF) @see SetPDUFormat for more info
  * @param pdu_specifier (PS) @see SetPDUSpecifier for more info
  * @param source_address of the frame
  * @param data of the can frame, vector of bytes, max size 8
  */
  Frame(const uint8_t priority, const bool data_page, const uint8_t pdu_format, 
    const uint8_t pdu_specifier, const uint8_t source_address, const std::vector<uint8_t> data); 

  /**
   * @param priority of the frame, is only 3 bits so can have values from 0...7
   * A value of 0 is highest prioritys.
   * @param pgn (Parameter group number (PGN)) - 18 bit that contains
   * reserved bit - data page bit - PF - PS
   * @param source_address of the frame
   * @param data of the can frame, vector of bytes, max size 8
  */
  Frame(const uint8_t priority, const uint32_t pgn, const uint8_t source_address, const std::vector<uint8_t> data);

  /**
   * @param header_data containing the 29 bits that j1939 structures use
   * @param data of the can frame, vector of bytes, max size 8
  */
  Frame(const uint32_t header_data, const std::vector<uint8_t> data);

  /**
   * Constuct from existing can_frame object
   * @param can_frame to extract data from
  */
  Frame(const can_frame& can_frame);

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        State                                   @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Check if PDU format is broadcast or peer-to-peer
   * This function does not take into cosideration the data page bit
   * @return true if broadcast, false if peer-to-peer
   */
  bool IsBroadCast() const;

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        Get & Set Functions                     @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Set the priority of the j1939 frame
   * @param priority, ranging from 0 highest and 7 lowest
   * @note function will clamp priority to a value between 0 - 7
  */
  void SetPriority(const uint8_t priority)
  {
    //Use mask to only get the last 3 bytes, thereby clamping value between 0 - 7
    priority_ = priority & static_cast<uint8_t>(0b111);
  }

  /**
   * Get the priority of the j1939 frame
   * @return priority of frame, 0 highest and 7 lowest
  */
  uint8_t GetPriority() const
  {
    return priority_;
  }

  /**
   * Set the data page bit to 0 or 1, the bit expands the PDU address by adding another bit of possible addresses
   * @param data_page_bit between 0 and 1, is clamped if needed.
  */
  void SetDataPageBit(const bool data_page_bit)
  {
    data_page_bit_ = (data_page_bit) ? 1 : 0;
  }

  /**
   * Get the data page bit
   * @return data page bit, 0 if not set, 1 if set
  */
  uint8_t GetDataPageBit() const
  {
    return data_page_bit_;
  }

  /**
  * @param pdu_format (PF), if the pdu format is 0 - 239 then the frame
  * is peer-to-peer communication and the PS is the destination adress.
  * If PF is 240-255 then the frame is broadcasted and PS is a PSU group extension.
  * SAE and Proprietary usage:
  * 0x00 − 0xEE: Peer-to-Peer messages defined by SAE
  * 0xEF – 0xEF: Peer-to-Peer message for proprietary use
  * 0xF0 – 0xFE: Broadcast messages defined by SAE
  * 0xFF – 0xFF: Broadcast messages for proprietary use
  */
  void SetPDUFormat(const uint8_t pdu_format)
  {
    pdu_format_ = pdu_format;
  }

  /**
   * Get the PDU Format data of the J1939 frame.
   * @return 8bit PDU Format 
  */
  uint8_t GetPDUFormat() const
  {
    return pdu_format_;
  }

  /**
  * @param pdu_specifier (PS) is either the destination address if the PDU format (PF) is 
  * a peer-to-peer massage or a PSU group extention if the PF is a broadcast message.
  */
  void SetPDUSpecifier(const uint8_t pdu_specifier)
  {
    pdu_specifier_ = pdu_specifier;
  }

  /**
   * Get the PDU Specifier data of the J1939 frame.
   * In the case that the message is peer-to-peer the PS will contain
   * the destination address of the frame
   * @return 8bit PDU Specifier
  */
  uint8_t GetPDUSpecifier() const
  {
    return pdu_specifier_;
  }

  /**
   * Set the source address of the can frame.
   * @param source_address of the frame 
   */
  void SetSourceAdderess(const uint8_t source_address)
  {
    source_address_ = source_address;
  }

  /**
   * Get the source address of the frame
   * @return 8 bit soruce address 
  */
  uint8_t GetSourceAdderess() const
  {
    return source_address_;
  }

  /**
   * Set the j1939 frame data vector
   * @param data vector, only the first 
  */
  void SetData(const std::vector<uint8_t> data)
  {
    ///NOTE: Could maybe be done simpler?
    data_.resize(std::min(data.size(), static_cast<size_t>(8)));
    for(uint8_t i = 0; i < data_.size(); i++)
    {
      data_[i] = data[i];
    }
  }

  /**
   * Get the 
  */
  std::vector<uint8_t> GetData() const
  {
    return data_;
  }

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        Get / Set combined data                 @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Get the 32-bit J1393 frame header with the can extended bit
   * rtr bit and error bit
   * @return 32-bit J1393 frame header
  */
  uint32_t GetHeader() const;

  /**
   * @todo find better name
   * Get the 29-bit J1393 frame header without can standard bits set
   * @return 29-bit J1393 frame header
  */
  uint32_t GetJ1939Header() const;

  /**
   * Get the 17-bit Parameter Group Number of the frame
   * @return 17-bit consisting of data page bit, PF 8-bit and PS 8-bit
  */
  uint32_t GetPGN() const;


  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//
  //@                        Convert                                 @//
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@//

  /**
   * Convert to linux can_frame structure
  */
  can_frame ToCanFrame() const;

private:

  ///TODO: Seriusly consider if using vector<bool> would be benefitial
  ///TODO: Create a base can frame to handle these:
  ///TODO: Probably make a can frame that contains the SOF, SRR, and IDE bits, 
  ///TODO: The RTR bit (remote request bit) is always set to zero in J1939.

  uint8_t priority_{};

  uint8_t data_page_bit_{};
  uint8_t pdu_format_{};        //PDU Format(PF) if PF is 0-239 then the frame peer-to-peer then PS is destination, if 240-255 then the frame is broadcasted
  uint8_t pdu_specifier_{};     //PDU specifier(PS)
  uint8_t source_address_{};    //Address of sender

  std::vector<uint8_t> data_{};
};

}