#pragma once

#include <cassert>
#include <cstdint>
#include <tuple>
#include <optional>

#include "j1939.h"
#include "frame.h"
#include "network.h"
#include "../socket.h"

namespace linsock::can::j1939
{

///TODO: Ensure that when searching the for avilable devices make sure that the local device
/// is also checked in the search

/**
 * Socket for sending/reciving CAN packets using j1939 protocol. 
 * Multiple sockets using CAN protocol can connect to the same socket
 * It is therefor recommended that user use a seperate socket for each of their
 * controller application
*/
class Socket
{
public:

  /**
   * @param
   * @param network for the specific device, nullptr not allowed.
  */
  Socket(const int fd, std::shared_ptr<Network> network, 
    const uint64_t bound_name );

  Socket(const int fd, std::shared_ptr<Network> network,
    const uint64_t bound_name, const uint64_t connected_name );

  //Copy
  Socket(const Socket &) = delete;
  Socket &operator=(const Socket &) = delete;

  //Move
  Socket(Socket &&) = default;
  Socket &operator=(Socket &&) = default;

  /// ##################### WRITE ##################### ///

  /**
   * Write a frame to socket without any checks
   * @param j1939_frame that will be sent
   * @return true if written false if an error occured
  */
  ssize_t WriteRaw(const Frame& j1939_frame);

  /**
   * Write a broadcast frame to the socket
   * @param j1939_frame that will be broadcast, the source address
   * is set by the socket
   * @return true if written false if an error occured
   * @throw std::invalid_argument if frame does not contain a
   * broadcast PDU_S or there is if the socket does not have
   * and address
   * are not available
  */
  ssize_t WriteBroadcast(Frame& j1939_frame);

  /**
   * Write frame to connected controller application
   * @param j1939_frame that will be sent, both source address
   * and PDU specifier is set by the socket
   * @return true if written, false if an error occured
   * @throw std::invalid_argument if no connected controller
   * app name has been set
  */ 
  ssize_t Write(Frame& j1939_frame);

  /**
   * Write frame to specific controller application
   * @param destination - name of the controller application to send to
   * @param j1939_frame that will be sent, both source address
   * and PDU specifier is set by the socket
   * @return true if written, false if an error occured
   * @throw std::invalid_argument if source and destination addresses
   * are not available
  */
  ssize_t WriteTo(const uint64_t destination, Frame& j1939_frame);

  /// ##################### READ ##################### ///

  /**
   * Read a frame from the socket
   * @return
  */
  virtual std::tuple<ssize_t, Frame> Read();

    /// ##################### Connection ##################### ///

  /**
   * Bind the controller application name to an address
   * Binding will first look for an available address, if the name already has an address
   * that address is returned, else if it finds an available address it will send an
   * address claim frame. If no deamon or address claiming thread is running. The user
   * is responsible for handling any counter claims.
   * @param preffered_address of the controller application
   * @param force - take address from lower priority device
   * @return address that was claimed, will be NULL_ADDRESS if none were available
   * or error
   * @todo add filters
  */
  uint8_t Bind(const uint8_t preffered_address, bool force = false);

  /**
   * Establish a peer to peer connection for this socket
   * with another controller application
   * @param name of controller application to establish connection to 
   * @return true if name has an address, false if name could not be found in network
   * @note use Write() funcion instead of WriteTo
   * after connecting
   * @todo Add filters
  */
  bool Connect(const uint64_t name);

  /**
   * Get bound name of the controller application
   * @return bound name, is 0 if no name is bound
  */
  uint64_t GetBoundName() const
  {
    return bound_name_;
  } 

  /**
   * @todo lock mutex?
   * Get name of the remote controller application this socket is connected to
   * @return connected name, is 0 if no name is bound
  */
  std::optional<uint64_t> GetConnectionName() const
  {
    return connection_name_;
  }

  std::shared_ptr<Network> GetNetwork() const
  {
    return ctrl_network_;
  }


  /// ##################### POLL ##################### ///

  bool IsOpen() const
  {
    return can_socket_.IsOpen();
  }

  /**
   * Check underlying can socket if there is any data
  */
  int Poll(const int number, const int time = 500) const;

  /// ##################### Filters ##################### ///

  /**
   * @todo Implement function, and implement own version can_filter
   */
  bool AddFilter(const std::vector<can_filter> filters);

private:
  linsock::can::Socket can_socket_;
  std::shared_ptr<Network> ctrl_network_{};

  //Optional
  const uint64_t bound_name_{};
  std::optional<uint64_t> connection_name_{};
};

}