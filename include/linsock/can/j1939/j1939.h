#pragma once

#include <cstdint>

///TODO: At a later stage make use of the linux header for J1939 once
///the linux for tegra version catches up to its implementation.

/**
 * Namespace containing static functions related to the j1939 protocol
*/
namespace linsock::can::j1939
{
  ///INFO: The data field of J1939 devices are written in LSB (little endian) format
  ///INFO: As such any constants needs to be reversed on big endian machines

  //Address Related consts
  constexpr uint8_t ADDRESS_GLOBAL{0xFFU};
  constexpr uint8_t ADDRESS_NULL{0xFEU};

  constexpr uint8_t J1939_ADDRESS_CLAIM{0xA0U};
  constexpr uint8_t J1939_ADDRESS_REQUEST{0xA1U};

  constexpr uint8_t PF_ADDRESS_CLAIM{0xEEU};
  constexpr uint32_t PGN_ADDRESS_CLAIM{0xEE'FFU};

  constexpr uint8_t PF_REQUEST{0xEAU};
  constexpr uint32_t PGN_REQUEST_ADDRESS{0xEA'FEU};

  constexpr uint8_t PF_ACKNOWLEDGE{0xE8U};
  constexpr uint32_t PGN_ACKNOWLEDGE{0xE8'FFU};

  constexpr uint32_t PGN_MASK{0x3'FF'FF};
  constexpr uint32_t PGN_PF_MASK{0x3'FF'00};

  ///NOTE: these are general purpose PDU's in the j1939
  //catalog but as we have not aquired it yet

  ///TODO: Maybe make name into its own class
  ///TODO: Include functions for getting Industy group, vehicle system instance, and more

  ///NOTE: May also need to include the ECU instance + manufacture code bytes
  constexpr uint64_t ECU_IDENTITY_MASK{0xFF'FF'FF'00'00'00'00'00LLU};

  /**
   * @todo Function causes issue with cmake as the function is defined in header
   * and in the usr/local/include this gets defined twice
  */
  //uint64_t GetECUIdentity(uint64_t name)
  //{
  //  return ECU_IDENTITY_MASK & name;
  //}
}