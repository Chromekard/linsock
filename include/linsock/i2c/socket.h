#pragma once

#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <cstdint>
#include <vector>

#include "linsock/socket.h"

namespace linsock::i2c
{

/**
 * Socket class that is used to intact with all i2c devices on the specified line 
*/
class Socket : public linsock::Socket
{
  public:

  /**
   * Constructor
  */
  Socket(const int fd) :  linsock::Socket(fd){}

  // ================================= SYSFS I/O ================================= //

  /**
   * @todo Implement
  */
  bool IO_Write();

  /**
   * @todo Implement
  */
  bool IO_Read();

  // ================================= IOCTL SMBUS ================================= //

  /** 
   * Sends a smbus ioctl data packet to write or read to a register depending on the data.
   * Examples of how to initalize data packet:
   * i2c_smbus_ioctl_data{ I2C_SMBUS_READ, register_address, I2C_SMBUS_BYTE_DATA, buff};
   * i2c_smbus_ioctl_data{ I2C_SMBUS_WRITE, register_address, I2C_SMBUS_WORD_DATA, buff};
   * @see /linux/i2c.h for more documentation on data structure
   * @param address of the device to connect to.
   * @param data to send in the form of a i2c_smbus_ioctl_data structure.
   * @param force a connection if the device is busy.
   * @return true if message is sent, false if not. Get error using errno.
  */
  bool SMBus_Send( const uint16_t address, i2c_smbus_ioctl_data& data, const bool force = false);

  // ================================= IOCTL Read/Write ================================= //

  /** 
   * Write an I2C message to a devices register with data.
   * @see /linux/i2c.h for more documentation on data structure
   * @param address of the device to connect to.
   * @param register_address to write to.
   * @param data to send to the device.
   * @return true if message is sent, false if not. Get error using errno.
   */
  bool I2C_Write(const uint16_t address, const uint8_t register_address, const uint8_t data);

  /** 
   * Write an I2C message to a devices register with data.
   * @see /linux/i2c.h for more documentation on data structure
   * @param address of the device to connect to.
   * @param register_address to write to.
   * @param data vector to send to the device.
   * @return true if message is sent, false if not. Get error using errno.
   */
  bool I2C_Write(const uint16_t address, const uint8_t register_address, const std::vector<uint8_t> data);

  /** 
   * Read an I2C register of a device.
   * @see /linux/i2c.h for more documentation on data structure
   * @param address of the device to connect to.
   * @param register_address to read from.
   * @param data read from device.
   * @return tuple with bool if read was done and byte read from register. Get error using errno.
   */
  bool I2C_Read(const uint16_t address, uint8_t register_address, uint8_t &data);

  /** 
   * Read an I2C register of a device.
   * @see /linux/i2c.h for more documentation on data structure
   * @param address of the device to connect to.
   * @param register_address to read from.
   * @param data vector read from device.
   * @return tuple with bool if read was done and vector of bytes read from register. Get error using errno.
   */
  bool I2C_Read(const uint16_t address, uint8_t register_address, std::vector<uint8_t> &data);

};

}