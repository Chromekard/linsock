#pragma once

#include <string>

#include <fcntl.h>

namespace linsock::i2c
{

  ///TODO: remove builder header and place into socket as namespace functions

  constexpr char I2C_PATH_PREFIX[]{"/dev/i2c-"};

  /**
   * @param num that the i2c socket will use, ex: /dev/i2c-{line}
   * @param flags to open the socket with. @see "linsock/flag_builder.h" for more, TODO: Implement flag builder
   * @return socket file descriptor, ret < 0 on error. Get error using errno.
  */
  int Build(const unsigned int num, const int flags)
  {
    return ::open((std::string{I2C_PATH_PREFIX} + std::to_string(num)).c_str(), flags);
  }

  /**
   * @param device_path that the i2c socket will use, ex: /dev/i2c-1
   * @param flags to open the socket with. @see "linsock/flag_builder.h" for more, TODO: Implement flag builder
   * @return socket file descriptor, ret < 0 on error. Get error using errno.
  */
  int Build(std::string device_path, const int flags)
  {
    return ::open(device_path.c_str(), flags);
  }

}