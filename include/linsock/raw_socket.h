#pragma once

#include "stream_socket.h"

namespace linsock
{

/**
 * Raw socket that implements wrapper for unistd read and write
*/
class RawSocket : public StreamSocket
{
public:

  RawSocket(const int fd) : StreamSocket(fd)
  {}

  /**
   * Wrapper for unistd write function
   * @param buffer to write, for containers use std::data()
   * @param nbytes in buffer, for containers use container.size()?
   * @return the number of bytes written, or -1
  */
  inline ssize_t Write(const void *buffer, size_t nbytes)
  {
    std::scoped_lock<std::mutex> lock(socket_mtx_);
    return ::write(fd_, buffer, nbytes);
  }

  ///TODO: Make sure std::data() works with read
  /**
   * Wrapper for unistd read function
   * @param buffer to write, for containers use std::data()
   * @param nbytes in buffer, use sizeof()
   * @return Return the number bytes read, or -1
  */
  inline ssize_t Read(void *buffer, size_t nbytes)
  {
    ///TODO: Does read need to be locked
    return ::read(fd_, buffer, nbytes);
  }

};

}