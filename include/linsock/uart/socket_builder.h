#pragma once

#include <string>

namespace linsock::uart
{

class SocketBuilder
{
public:

  SocketBuilder SetTTYPath(const std::string tty_path)
  {
    tty_path_ = tty_path;
    return *this;
  }

  int Build(const int flags);

private:
  std::string tty_path_{"/dev/ttyTHS1"};
};

}