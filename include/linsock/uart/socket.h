#pragma once

#include <string>
#include <cstdint>
#include <vector>
#include <tuple>

#include "linsock/stream_socket.h"

namespace linsock::uart
{

/**
 * Socket class that is used to intact with uart connections on a specific rx/tx channel
*/
class Socket : public linsock::StreamSocket
{
public:

  /**
   * Constructor
  */
  Socket(const int fd) :  linsock::StreamSocket(fd){}

  ///TODO: Implement write
  ///TODO: Or is it possible to just use the raw socket instead?

  /**
   * Read bytes from socket
   * @param count of bytes to read, defualt 255
   * @return tuple containing ssize_t with the amount of bytes read, -1 for errors or 0 for EOF. 
   * Also a vector of bytes read. Get error using errno.
  */
  std::tuple<ssize_t , std::vector<uint8_t>> ReadBytes(unsigned int count = 255)
  {
    ///TODO: Possibly have buffer be a memeber to stop creating it everytime
    std::vector<uint8_t> buffer(count, 0);

    //tcflush(fd_, TCIOFLUSH); // Cant use flush here as it will destroy the remaining data
    //std::this_thread::sleep_for(std::chrono::milliseconds(1));

    return {::read(fd_, std::data(buffer), count), buffer};
  }
};

}